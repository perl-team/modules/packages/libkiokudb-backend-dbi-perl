libkiokudb-backend-dbi-perl (1.23-3) unstable; urgency=medium

  * debian/watch: use uscan version 4.
  * Make build dependency on libkiokudb-perl versioned. (Closes: #952229)
  * Update years of packaging copyright.
  * Simplify alternative test dependencies.
  * Declare compliance with Debian Policy 4.6.1.
  * Bump debhelper-compat to 13.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Add debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Tue, 24 May 2022 10:23:34 +0200

libkiokudb-backend-dbi-perl (1.23-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Tim Retout from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update versioned build dependency on libtest-simple-perl et al.
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Fri, 30 Mar 2018 11:50:43 +0300

libkiokudb-backend-dbi-perl (1.23-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of copyright.
  * Update (build) dependencies.
  * Don't install a new, syntactically incorrect, and almost empty
    manpage.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Tue, 06 May 2014 20:25:08 +0200

libkiokudb-backend-dbi-perl (1.22-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Add (build) dependency on liblist-moreutils-perl.
  * Update years of copyright.
  * Drop patch fix-typos, merged upstream.
  * Declare compliance with Debian Policy 3.9.4.
  * Make URI in Vcs-Git canonical.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Oct 2013 20:14:11 +0200

libkiokudb-backend-dbi-perl (1.21-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Remove some unnecessary versions from (build) dependencies.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Wed, 05 Oct 2011 19:30:57 +0200

libkiokudb-backend-dbi-perl (1.20-1) unstable; urgency=low

  * Initial Release. (Closes: #635638)

 -- Tim Retout <diocles@debian.org>  Tue, 26 Jul 2011 07:06:29 +0100
